import {loginEmail, loginPasswod} from '../../secrets'

describe('Dashboard Test Suite',() =>{
    it('Check dashboard subtile equals Today', () =>{
        cy.url().should('contain', '/customer/dashboard')
        cy.get('.self-center > .dashboard-subtitle').should('contain.text', 'Today')
    })

    it('Checking Active Propperties, Active Internet Users, GB Usage components', () =>{
        cy.get('[data-cy="dashboard-overview-usage-stats-1"]').should('be.visible')
        cy.get('[data-cy="dashboard-overview-usage-stats-2"]').should('be.visible')
        cy.get('[data-cy="dashboard-overview-usage-stats-3"]').should('be.visible')
    })

    it('Checking 2nd dashboard subtile exists & equals Overview', () =>{
        cy.get('.col-12 > .dashboard-subtitle').should('contain.text', 'Overview')

    })

    it('Checking Property type list has Hotel, Student Accom, Residential', () =>{
        cy.get('.col-4 > .q-field > .q-field__inner > .q-field__control').click()
        cy.get('#qvs_1').should('contain', 'Hotel')
        cy.get('#qvs_1').should('contain', 'Student Accommodation')
        cy.get('#qvs_1').should('contain', 'Residential')

    })

    it('Checking Active Propperties, Active Internet Users, GB Usage components', () =>{
                cy.get('.col-12 > .dashboard-subtitle').should('contain.text', 'Overview')

    })


    it('Accounts Activated, Total # of Devices and GB Usage Total components exist',() =>{

        cy.scrollTo('bottom')
        cy.get('[data-cy="totalAccountsActivatedChart"]', {timeout: 3000}).should('be.visible')
        cy.get('[data-cy="totalDevicesChart"]').should('be.visible')
        cy.get('[data-cy="totalDevicesChart"]').should('be.visible')
    })
})
