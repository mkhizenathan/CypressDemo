import {loginEmail, loginPasswod, groupTestEmail} from '../../secrets'

describe('Group Test Suite',() =>{
    
    /*  Use a unique name everything this is run because the database stores name even
        after the group is deleted. Field is set to not accept duplicates
        Example: Change - "SandileCC My New Test Group" to "SandileZZ My New Test Group"
        I have named it this way for Devs easily delete unwanted DB names matching name "Sandile"
        */
    var name = "SandileAAH My New Test Group"

    it('Creating new group with specified conditions & Hardypol Portal',() =>{
        cy.get(':nth-child(2) > .col-12 > .dash-menu-item').click()
        cy.get('.top-menu-button > .q-btn > .q-btn__wrapper > .q-btn__content').click()
        cy.get('[data-cy="name"]').clear().type(name)
        cy.get('[data-cy="contact-email"]').clear().type(groupTestEmail)
        cy.get('.q-stepper__nav > .q-btn > .q-btn__wrapper').click()
        cy.get('.later-button > .q-btn__wrapper > .q-btn__content > .block').click()
        cy.get('.q-field__control-container').click()  
            
        cy.wait(4000)
        cy.get('#qvs_8').contains('Hardypol').should('exist').click()
        cy.get('.q-stepper__nav > .q-btn > .q-btn__wrapper > .q-btn__content').click()
        cy.get('.later-button > .q-btn__wrapper > .q-btn__content > .block').click()
    })

    it('Checking New Group is displayed', () =>{
        cy.get(':nth-child(2) > .col-12 > .dash-menu-item').click()
        cy.wait(500)
        cy.get('.q-field__control').click().clear().type(name +'{enter}')
        cy.wait(500)
        cy.contains(name).should('be.visible')
    })

    // For some strange reason this this test case fails when running with the other but when run alone passes as it should.
    it('Testing Group value is set to 0', () =>{
        cy.wait(1000)
        cy.get(':nth-child(2) > .col-12 > .dash-menu-item').click()
        cy.get('.q-field__control').click().clear().type(name)
        cy.get('[style="min-width: 100px; text-transform: capitalize;"]').should('contain', '0')
    })

    it('Testing Group status is set to DRAFT', () =>{
        cy.get(':nth-child(2) > .col-12 > .dash-menu-item').click()
        cy.get('.q-field__control').click().type(name +'{enter}')
        cy.wait(1000)
        cy.get('.q-badge').should('contain', 'draft')        
    })

    /*
    
    NOTE!!!
    In order to run this test you will need to change the name var again for  reasons stated above.
    It will fail because there is no IO Portal
    */
    it('Creating new group with specified conditions & IO Portal',() =>{
        cy.get(':nth-child(2) > .col-12 > .dash-menu-item').click()
        cy.get('.top-menu-button > .q-btn > .q-btn__wrapper > .q-btn__content').click()
        cy.get('[data-cy="name"]').clear().type(name)
        cy.get('[data-cy="contact-email"]').type(groupTestEmail)
        cy.get('.q-stepper__nav > .q-btn > .q-btn__wrapper').click()
        cy.get('.later-button > .q-btn__wrapper > .q-btn__content > .block').click()
        cy.get('.q-field__control-container').click()

        cy.wait(4000)
        cy.get('#qvs_8').contains('IO Digital').should('exist').click()
        cy.get('.q-stepper__nav > .q-btn > .q-btn__wrapper > .q-btn__content').click()
        cy.get('.later-button > .q-btn__wrapper > .q-btn__content > .block').click()
    })
})
