

describe('Properties Test Suite',() =>{
    it('Creating New Property With Defined Values',() =>{
        cy.visit('/customer/properties')
        cy.wait(11000)
        cy.get('.top-menu-button > .q-btn > .q-btn__wrapper > .q-btn__content').click()
        cy.get('[data-cy="group-id"]').click()
        // Change contains to updated name in groups.spec
        cy.get('#qvs_1').contains('SandileAAG My New Test Group').should('exist').click()
        cy.get(':nth-child(1) > .q-input > .q-field__inner > .q-field__control > .q-field__control-container > [data-cy="max-number-of-accounts"]').click().clear().type('My Test Hotel')
        cy.get('[data-cy="property-type"]').click()
        cy.contains('Hotel').click()
        cy.get('[data-cy="external-reference"]').clear().type('#1234')
        cy.get('[data-cy="package-id"]').click('')
        cy.contains('10GB / 10Mbs').should('be.visible').click()
    })

    // Authentiction step fails here as no details are provided in the PDF
    it('Testing Property Details', () => {
        cy.visit('/customer/properties')
        cy.wait(11000)
        cy.get('.top-menu-button > .q-btn > .q-btn__wrapper > .q-btn__content').click()
        cy.get('[data-cy="group-id"]').click()

        // Change contains to updated name in groups.spec
        cy.get('#qvs_1').contains('SandileAAG My New Test Group').should('exist').click()
        cy.get(':nth-child(1) > .q-input > .q-field__inner > .q-field__control > .q-field__control-container > [data-cy="max-number-of-accounts"]').click().clear().type('My Test Hotel')
        cy.get('[data-cy="property-type"]').click()
        cy.contains('Hotel').click()
        cy.get('[data-cy="external-reference"]').type('#1234')
        cy.get('[data-cy="package-id"]').click('')
        cy.contains('office test 1').should('be.visible').click()
        
        cy.get('[data-cy="send-email"]').click()
        cy.get('[data-cy="send-sms"]').click()
        
        cy.get('[data-cy="street-address"]').clear().type('1 Cheese Ave')
        cy.get('[data-cy="city"]').clear().type('Pizza')
        cy.get('[data-cy="zip-code"]').clear().type('1234')
        cy.get('.q-field__native > [data-cy="country"]').click()
        cy.contains('Angola').should('be.visible').click()
        cy.get('[data-cy="email"]').clear().type('testing@tester.com')
        cy.get('.q-stepper__nav > .q-btn > .q-btn__wrapper > .q-btn__content').click()


    })

    // Running test with dummp authentication values
    it.only('Testing Gateway with Authentication Step', () =>{
        cy.visit('/customer/properties')
        cy.wait(11000)
        cy.get('.top-menu-button > .q-btn > .q-btn__wrapper > .q-btn__content').click()
        cy.get('[data-cy="group-id"]').click()

        // Change contains to updated name in groups.spec
        cy.get('#qvs_1').contains('SandileAAG My New Test Group').should('exist').click()
        cy.get(':nth-child(1) > .q-input > .q-field__inner > .q-field__control > .q-field__control-container > [data-cy="max-number-of-accounts"]').click().clear().type('My Test Hotel')
        cy.get('[data-cy="property-type"]').click()
        cy.contains('Hotel').click()
        cy.get('[data-cy="external-reference"]').clear().type('#1234')
        cy.get('[data-cy="package-id"]').click('')
        cy.contains('office test 1').should('be.visible').click()
        
        cy.get('[data-cy="send-email"]').click()
        cy.get('[data-cy="send-sms"]').click()
        
        cy.get('[data-cy="street-address"]').clear().type('1 Cheese Ave')
        cy.get('[data-cy="city"]').type('Pizza')
        cy.get('[data-cy="zip-code"]').type('1234')
        cy.get('.q-field__native > [data-cy="country"]').click()
        cy.contains('Angola').should('be.visible').click()
        cy.get('[data-cy="email"]').clear().type('testing@tester.com')
        cy.get('.q-stepper__nav > .q-btn > .q-btn__wrapper > .q-btn__content').click()

        //Authentication Step
        cy.get('[data-cy="connectivity-type-id"]').click().wait
        cy.get('#qvs_7').contains('FTTH').should('exist').click()
        cy.get('.finish-button-black > .q-btn__wrapper > .q-btn__content').click()
        
        //Gateway
        cy.get('[data-cy="gateway-type-id"]').click()
        cy.contains('Mikrotik').should('be.visible').click()
        cy.get('[data-cy="gateway-type-id"]').clear().type('2C:54:91:88:C9:E3')  
        cy.get('[data-cy="ip-address"]').clear().type('17.5.7.8')
        cy.get('[data-cy="dns-name"]').clear().type('mytest.com')

        // The Test should fail here because the continue button is not clickable
        // I should compare that URL has changed to make test case fail
        cy.get('[data-cy="username"]').type('Elon')
        cy.get('[data-cy="password"]').type('dxvI33ei;8')
        cy.get('.finish-button > .q-btn__wrapper > .q-btn__content').click()

    })
})
