import {loginEmail, loginPasswod, groupTestEmail} from '../../secrets'

// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

before(() => {
    cy.log('Starting')
  })

beforeEach(() => {
    cy.log('I run before every test in every spec file!!!!!!')
    cy.visit('/');
    cy.get('[data-cy="signin-email-input"]').clear().type(loginEmail)
    cy.get('[data-cy="signin-password-input"]').clear().type(loginPasswod)
    cy.get('.q-btn__content').click()
    cy.wait(11000) // Ask about the waiting API issues
  })
  

  afterEach(() =>{
    // Some strange reason it only works with a double click
    cy.get('.q-toolbar > .q-btn > .q-btn__wrapper > .q-btn__content').click()
    cy.get('.q-toolbar > .q-btn > .q-btn__wrapper > .q-btn__content').click()
    cy.get(':nth-child(9) > .q-item__section').click()

 })
