
# Mini-Automation Testing Project

A 3 day mini project to assess if the Automation Tester can do the basics expected within an appropriate time frame

## Requirements

- Install Cypress.io
- Run Cypress on Chrome 99 browser


## Test Summary

**Project Name:** Mini-Automation Testing Project

**Project Timeframe:** 3 Days

**Additional Comments:** "Doing this project in a new framework outside my comfort zone was a great learning curve. Learning to not overthink solutions with this framework with the guidance from IO team members has helped me up my skills. Even though I would have liked to have finished the project completely, I am glad to have been given the advice to take my time and dive into concepts to better understand the framework and learn"

**NOTE:** The only test critera I did not push in my final code is Aliasing Elements. I broke some of my logic when I refractoring so I chose not to push.
## Summary

Project Name: Mini-Automation Testing Project
Version Number: 1.0
Test Automation Engineer: Sandile Mkhize
Project Timeframe: 3 Days 
Additional Comments:


## Comment Documentation

Instead of the initial documentation of complete test cases in table form, after a chat with Michael I decided to best explain all documentation within this ReadMe and in the code comments.
## Issues/Errors

- When attempting to create a group. You cannot create a group with the same name. Even after groupo is DELETED. "No name duplicates allowed"
- Cannot delete a Draft group
- As you will notice documention in the comments. Dummy values and new steps where created to to pass failed tests to test other components. 


## Recommendations
#### Login Test
- Implement a DATA DRIVEN automated script to test multiple username & email combinations that test both accepted & unaccepted combinations. This is normally done when running locally to speed up the process.

#### Group Drafts
- Users should have the ability to delete Draft Groups the same way Draft Prjects are able to be deleted.
### Special Notes:
- When implementing the Log Out steps, when trying to replicate a “mouse hover” over the top right menu element, a work around that worked is double clicking the element. Why this works I do not know. For future reference a Cypress implementation should be figured out and testers should ask the developers how they implemented that particular element(hover.hidden elements). This will help solve future element identification problems.

- Sometimes the tests do run pass when the entire test suite is run. When set to it.only it passes. Not sure why this happens. I suspect I am using Cypress incorrectly.
